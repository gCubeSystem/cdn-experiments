class CCPOutputWidgetController extends HTMLElement {
  
  #output = null;
  
  constructor(){
    super()
    this.#output = JSON.parse(this.getAttribute("output"))
  }
  
  connectedCallback(){
    this.innerHTML = this.render()
  }
  
  get name(){
    return this.#output.id
  }
  
  get enabled(){
    return this.querySelector("input").checked
  }
  
  render(){
    return `
      <div class="col form-check">
        <input class="form-check-input" type="checkbox" name="this.#output.id" alt="${this.#output.description}" title="${this.#output.description}" checked="checked"/>
        <label class="form-check-label">${this.#output.title}</label>
      </div>
    `
  }
}
window.customElements.define('d4s-ccp-output', CCPOutputWidgetController);