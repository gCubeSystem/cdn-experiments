class CCPInputWidgetEditorController extends HTMLElement{
  
  #input = null
  #index = null
  #type = null
  
  #delete_icon = `
    <svg style="width:24px;height:24px;pointer-events: none;" viewBox="0 0 24 24">
      <path d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" />
    </svg>
  `
    
  constructor(){
    super();
    
  }
  
  connectedCallback(){
    
  }
  
  get index(){
    return this.#index
  }
  
  computeDefaultInputType(){
    if(this.#input.schema.format === "secret"){
       return "password"
    }else if(this.#input.schema.format === "date"){
      return "date"
    }else if(this.#input.schema.format === "time"){
      return "time"
    }else if(this.#input.schema.format === "dateTime"){
      return "datetime-local"
    }
    return "text"
  }
  
  isSelectedFormat(fmt){
    return this.#input.schema.format === fmt  
  }
 
  renderDefaultByType(){
    if(this.#input.schema.format === "code"){
      return `
        <textarea rows="5" name="default" class="form-control" placeholder="default">${this.#input.schema.default}</textarea>
      `
    } else {
      return `
        <input type="${this.computeDefaultInputType()}" value="${this.#input.schema.default}" name="default" class="form-control" placeholder="default"/>	    
      `   
    }
  }

  renderDeleteButton(){
    return  `<button data-index="${this.#index}" name="delete-input" title="Delete" class="btn btn-danger ccp-toolbar-button">
      ${this.#delete_icon} 
    </button>`
  }

  render(input, i, reopen){
    this.#index = i
    this.#input = input
    this.#type = input.schema.enum ? "enum" : "string"
    this.innerHTML = `
        <details ${ reopen ? 'open' : ''}>
          <summary class="mb-3">
            <input class="form-control" style="width:auto;display:inline" required="required" name="id" value="${input.id}" title="Id of input" ${ input.id === 'ccpimage' ? 'readonly' : ''}/>
            ${ input.id !== 'ccpimage' ? this.renderDeleteButton() : ''}  
          </summary>
          <div style="padding-left: 1rem;border-left: 1px solid gray;">
            <div class="row mb-3">
              <div class="col">
                <div class="form-field" title="Title">
                  <input name="title" class="form-control" placeholder="title" value="${input.title}" required="required" ${ input.id === 'ccpimage' ? 'readonly' : ''}/>
                </div>
              </div>
            </div>
            <div class="mb-3">
              <div class="form-field" title="description">
                <input name="description" class="form-control" placeholder="description" value="${input.description}" required="required" ${ input.id === 'ccpimage' ? 'readonly' : ''}/>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-3">
                <div class="form-field" title="Type">
                  <label>Type</label>
                  <select name="type" class="form-control" placeholder="type" value="${this.#type}" ${ input.id === 'ccpimage' ? 'readonly' : ''}>
                    <option value="string" ${this.#type === "string" ? "selected" : ""}>String</option>
                    <option value="enum" ${this.#type === "enum" ? "selected" : ""}>Enum</option>
                  </select>
                </div>
              </div>
              <div class="col">
                <div class="form-field" title="Minimum occurrences">
                  <label>Min occurs</label>
                  <input value="${input.minOccurs}" type="number" min="0" step="1" name="minOccurs" value="${input.minOccurs}" required="required" class="form-control" placeholder="minOccurs" ${ input.id === 'ccpimage' ? 'readonly' : ''}/>
                </div>
              </div>
              <div class="col">
                <div class="form-field" title="Maximum occurrences">
                  <label>Max occurs</label>
                  <input value="${input.maxOccurs}" type="number" min="0" step="1" name="maxOccurs" value="${input.maxOccurs}" required="required" class="form-control" placeholder="maxOccurs" ${ input.id === 'ccpimage' ? 'readonly' : ''}/>
                </div>
              </div>
              <div class="col" title="Read only">
                <label>Read only</label>
                <div class="form-field">
                  <input type="checkbox" ${input.schema.readOnly ? 'checked' : ''} name="readonly" class="form-check-input">
                </div>
              </div>
            </div>
            <div name="string-input" class="${this.#type === 'enum' ? 'd-none' : ''} row mb-3">
              <div class="col-3">
                <div class="form-field" title="Format">
                  <label>Format</label>
                  <select value="${input.schema.format}" name="format" class="form-control" ${ input.id === 'ccpimage' ? 'readonly' : ''}>
                    <option value="none" ${this.isSelectedFormat('none') ? "selected" : ""}>None</option>
                    <option value="date" ${this.isSelectedFormat('date') ? "selected" : ""}>Date</option>
                    <option value="time" ${this.isSelectedFormat('time') ? "selected" : ""}>Time</option>
                    <option value="dateTime" ${this.isSelectedFormat('dateTime') ? "selected" : ""}>Date time</option>
                    <option value="number" ${this.isSelectedFormat('number') ? "selected" : ""}>Number</option>
                    <option value="boolean" ${this.isSelectedFormat('boolean') ? "selected" : ""}>True/False</option>
                    <option value="code" ${this.isSelectedFormat('code') ? "selected" : ""}>Code</option>
                    <option value="file" ${this.isSelectedFormat('file') ? "selected" : ""}>File</option>
                    <option value="remotefile" ${this.isSelectedFormat('remotefile') ? "selected" : ""}>Remote file</option>
                    <option value="secret" ${this.isSelectedFormat('secret') ? "selected" : ""}>Secret</option>
                    <option value="url" ${this.isSelectedFormat('url') ? "selected" : ""}>Url</option>
                  </select>
                </div>
              </div>
              <div class="col" title="Mime type">
                <label>Mime</label>
                <div class="form-field">
                  <input value="${input.schema.contentMediaType}" name="contentMediaType" class="form-control" placeholder="mime" ${ input.id === 'ccpimage' ? 'readonly' : ''}/>
                </div>
              </div>
            </div>
            <div name="enum-input" class="${this.#type !== 'enum' ? 'd-none' : ''} mb-3">
              <div class="form-field" title="options">
                <input name="options" class="form-control" type="text" placeholder="option" value="${input.schema.enum ? input.schema.enum.join(',') : ''}"/>
                <small class="form-text text-muted">Comma separated list of options</small>
              </div>
              <div class="form-field" title="Format">
                <label>Format</label>
                <select value="${input.schema.format}" name="format" class="form-control">
                  <option value="select" ${this.isSelectedFormat('select') ? "selected" : ""}>Choice</option>
                  <option value="checklist" ${this.isSelectedFormat('checklist') ? "selected" : ""}>Multi Choice</option>
                </select>
              </div>
            </div>
            <div name="input-default" class="mb-3">
              <div class="form-field" title="Default value">
                ${this.renderDefaultByType()}
                <span style="user-select:none;position:relative;top:-1.6rem;float:right;cursor:pointer" name="password_toggle" class="${this.isSelectedFormat('secret') ? 'inline' : 'd-none'}">&#128065;</span>
              </div>
            </div>
          </div>
        </details>
    `    
    this.addEventListener("click", ev=>{
      const ename = ev.target.getAttribute("name")
      if(ename === "password_toggle"){
        const w = this.querySelector("div[name=input-default] input[name=default]")
        w.type = (w.type === "password" ? "" : "password")
        ev.preventDefault()
      }
    })
    
    this.addEventListener("input", ev=>{
      const val = ev.target.value
      const ename = ev.target.getAttribute("name")
      if(ename === "id"){
        this.#input.id = val
      }
      else if(ename === "title"){
        this.#input.title = val
      }
      else if(ename === "description"){
        this.#input.description = val
      }
      else if(ename === "minOccurs"){
        this.#input.minOccurs = val
      }
      else if(ename === "maxOccurs"){
        this.#input.maxOccurs = val
      }
      else if(ename === "format"){
        this.#input.schema.format = val
        //this.render(this.#input, this.#index, true)
	/*this.querySelector("div[name=input-default] span[name=password_toggle]").classList.add("d-none")
        this.querySelector("div[name=input-default] input[name=default]").type = ""
        if(this.#input.schema.format === "secret"){
           this.querySelector("div[name=input-default] input[name=default]").type = "password"
           this.querySelector("div[name=input-default] span[name=password_toggle]").classList.remove("d-none")
        }else if(this.#input.schema.format === "date"){
          this.querySelector("div[name=input-default] input[name=default]").type = "date"
        }else if(this.#input.schema.format === "time"){
          this.querySelector("div[name=input-default] input[name=default]").type = "time"
        }else if(this.#input.schema.format === "dateTime"){
          this.querySelector("div[name=input-default] input[name=default]").type = "dateTime"
        }else if(this.#input.schema.format === "file"){
          this.querySelector("div[name=input-default] input[name=default]").type = "file"
        }*/
      }
      else if(ename === "contentMediaType"){
        this.#input.schema.contentMediaType = val
      }
      else if(ename === "options"){
        this.#input.schema.enum = val.split(",")
      }
      else if(ename === "default"){
        this.#input.schema.default = val
      }
      else if(ename === "readonly"){
        this.#input.schema.readOnly = ev.target.checked
      }
      else if(ename === "type"){
        this.#type = ev.target.value
        if(this.#type === "enum"){
          this.querySelector("div[name=string-input]").classList.add("d-none")
          this.querySelector("div[name=enum-input]").classList.remove("d-none")
          this.#input.schema.enum = this.querySelector("input[name=options]").value.split(",")
        }else if(this.#type === "string"){
          this.querySelector("div[name=enum-input]").classList.add("d-none")
          this.querySelector("div[name=string-input]").classList.remove("d-none")
          delete this.#input.schema['enum']
        } 
      } 
    })
    
  }
  
}
window.customElements.define('d4s-ccp-input-editor', CCPInputWidgetEditorController);
